# Generated by Django 2.2.5 on 2019-09-29 06:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0004_auto_20190929_0634'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pagetocontent',
            name='ordering',
            field=models.IntegerField(default=0),
        ),
    ]
