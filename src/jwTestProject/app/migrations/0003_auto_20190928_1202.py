# Generated by Django 2.2.5 on 2019-09-28 12:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0002_auto_20190928_0952'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='pagetocontent',
            name='counter',
        ),
        migrations.AddField(
            model_name='contentitem',
            name='counter',
            field=models.BigIntegerField(default=0),
        ),
    ]
