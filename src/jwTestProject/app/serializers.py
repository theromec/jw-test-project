from rest_framework import serializers
from rest_framework.reverse import reverse

from app.models import Page, PAGE_CONTENT_ITEM_TYPES


def content_item_serializer_class_factory(model_class):
    class ModelSerializer(serializers.ModelSerializer):
        content_type = serializers.SerializerMethodField()

        class Meta:
            model = model_class
            fields = model_class.get_api_fields() + ('content_type',)

        def get_content_type(self, _):
            return model_class.get_content_type()

    return type(
        '{}ModelSerializer'.format(model_class.__name__),
        (ModelSerializer,),
        dict()
    )


def get_content_item_serializers(model_classes):
    result = dict()
    for model_class in model_classes:
        content_type = model_class.get_content_type()
        result[content_type] = content_item_serializer_class_factory(model_class)
    return result


class PageSerializer(serializers.ModelSerializer):
    contents = serializers.SerializerMethodField()
    resource_url = serializers.SerializerMethodField()

    class Meta:
        model = Page
        fields = ('title', 'contents', 'resource_url',)
        content_item_serializers = get_content_item_serializers(PAGE_CONTENT_ITEM_TYPES)

    def _get_class(self, content_item_type):
        return self.Meta.content_item_serializers[content_item_type]

    def get_resource_url(self, obj):
        request = self.context.get('request')
        return reverse('api_v1_page-detail', args=[obj.pk], request=request)

    def get_contents(self, obj):
        page_to_content_items = sorted(obj.pagetocontent_set.all(), key=lambda x: x.ordering)
        serialized_items = [
            self._get_class(item.content_item_type)(instance=getattr(item.content, item.content_item_type)).data
            for item in page_to_content_items
        ]
        return serialized_items
