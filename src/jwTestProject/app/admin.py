from django.contrib import admin

from app.models import Page, PageToContent, \
    ContentItem, VideoContent, AudioContent, TextContent, NewContentX


class PageToContentInline(admin.TabularInline):
    model = PageToContent
    extra = 0
    exclude = ('content_item_type',)
    autocomplete_fields = ('content',)


class AdminContentItemBase(admin.ModelAdmin):
    model = ContentItem
    readonly_fields = ('counter',)
    search_fields = ('title',)


class PageAdmin(admin.ModelAdmin):
    model = Page
    inlines = [
        PageToContentInline,
    ]
    exclude = ('contents',)
    search_fields = ('title',)


admin.site.register(Page, PageAdmin)
admin.site.register(ContentItem, AdminContentItemBase)
admin.site.register(VideoContent, AdminContentItemBase)
admin.site.register(AudioContent, AdminContentItemBase)
admin.site.register(TextContent, AdminContentItemBase)
admin.site.register(NewContentX, AdminContentItemBase)
