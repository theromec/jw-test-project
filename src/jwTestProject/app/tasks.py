from __future__ import absolute_import, unicode_literals

from celery import shared_task
from django.db.models import F

from app.models import ContentItem


@shared_task
def update_counters(content_ids):
    if content_ids:
        ContentItem.objects.filter(pk__in=content_ids).update(counter=F('counter') + 1)
