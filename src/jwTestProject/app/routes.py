from rest_framework.routers import DefaultRouter

from app import views

api_v1 = DefaultRouter()
api_v1.register(r'pages', views.PageViewSet, base_name='api_v1_page')
