from django.core.exceptions import ValidationError
from django.db import models

#
# Чтобы добавить новый тип контента достаточно:
# - добавить модель, унаследовать ее от ContentItem
# - перечислить поля, которые могут читаться через API, в свойстве _api_fields
# - добавить класс модели в список PAGE_CONTENT_ITEM_TYPES
# - зарегистрировать модель в admin.py
#


class ContentItem(models.Model):
    title = models.CharField(max_length=100)
    counter = models.BigIntegerField(default=0)

    _api_fields = (
        'title',
    )

    @classmethod
    def get_api_fields(cls):
        return ContentItem._api_fields + cls._api_fields

    @classmethod
    def get_content_type(cls):
        return cls.__name__.lower()

    def __str__(self):
        return f'# {self.pk} - {self.__class__.__name__} - {self.title}'


def validate_file_size(value):
    file_size = value.size
    limit = 5
    if file_size > limit * 1000 * 1000:
        raise ValidationError(f'The maximum file size that can be uploaded is {limit}MB')
    else:
        return value


class VideoContent(ContentItem):
    video = models.FileField(validators=[validate_file_size])
    subtitles = models.FileField(validators=[validate_file_size])
    _api_fields = (
        'video',
        'subtitles',
    )


class AudioContent(ContentItem):
    bitrate = models.IntegerField()
    _api_fields = (
        'bitrate',
    )


class TextContent(ContentItem):
    text = models.TextField()
    _api_fields = (
        'text',
    )


class NewContentX(ContentItem):
    x = models.CharField(max_length=100)
    _api_fields = (
        'x',
    )


PAGE_CONTENT_ITEM_TYPES = (
    VideoContent,
    AudioContent,
    TextContent,
    NewContentX,
)


class Page(models.Model):
    title = models.CharField(max_length=100)
    contents = models.ManyToManyField(ContentItem, through='PageToContent')

    def __str__(self):
        return f'# {self.pk} - {self.__class__.__name__} - {self.title}'


class PageToContent(models.Model):
    page = models.ForeignKey(Page, on_delete=models.CASCADE)
    content = models.ForeignKey(ContentItem, on_delete=models.CASCADE)
    ordering = models.IntegerField(default=0)
    content_item_type = models.CharField(max_length=255)

    class Meta:
        unique_together = (
            ('page', 'content',),
        )
        ordering = ('ordering',)

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        for content_type in PAGE_CONTENT_ITEM_TYPES:
            try:
                concrete_content = getattr(self.content, content_type.get_content_type())
                self.content_item_type = concrete_content.__class__.get_content_type()
                break
            except models.ObjectDoesNotExist:
                pass

        return super(PageToContent, self).save(force_insert, force_update, using, update_fields)

    def __str__(self):
        return f'# {self.pk} - {self.content.__class__.__name__} - {self.ordering}'
