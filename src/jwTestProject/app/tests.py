from unittest.mock import patch

from django.test import TestCase
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase

from app.models import VideoContent, Page, PageToContent, AudioContent, TextContent
from app.serializers import PageSerializer
from app.tasks import update_counters
from app.views import get_queryset


class ModelTest(TestCase):
    def test_create_content(self):
        video = VideoContent()
        video.save()
        self.assertIsNotNone(video.pk)

    def test_create_page(self):
        page = Page()
        page.save()
        self.assertIsNotNone(page.pk)

    def test_create_page_to_content(self):
        page = Page()
        page.save()
        video = VideoContent()
        video.save()
        page_to_content = PageToContent(page=page, content=video)
        page_to_content.ordering = 1
        page_to_content.save()
        self.assertIsNotNone(page_to_content.pk)
        self.assertEqual('videocontent', page_to_content.content_item_type)


class SerializerTest(TestCase):
    def test_page_serializer(self):
        page = Page()
        page.save()
        video = VideoContent()
        video.save()
        page_to_content = PageToContent(page=page, content=video)
        page_to_content.ordering = 1
        page_to_content.save()

        serializer = PageSerializer(instance=page)

        self.assertEqual(
            {
                'title': '',
                'contents': [
                    {
                        'title': '',
                        'video': None,
                        'subtitles': None,
                        'content_type': 'videocontent'
                    },
                ],
                'resource_url': f'/api/v1/pages/{page.pk}/',
            },
            serializer.data
        )

    def test_page_serializer_ordering_multiple_contents(self):
        page = Page()
        page.save()
        video1 = VideoContent(title='video1')
        video1.save()
        video2 = VideoContent(title='video2')
        video2.save()
        audio = AudioContent(title='audio', bitrate=128)
        audio.save()
        text = TextContent(title='text')
        text.save()
        page_to_video1 = PageToContent(page=page, content=video1, ordering=4)
        page_to_video1.save()
        page_to_video2 = PageToContent(page=page, content=video2, ordering=3)
        page_to_video2.save()
        page_to_audio = PageToContent(page=page, content=audio, ordering=2)
        page_to_audio.save()
        page_to_text = PageToContent(page=page, content=text, ordering=1)
        page_to_text.save()

        serializer = PageSerializer(instance=page)

        self.assertEqual(
            {
                'title': '',
                'contents': [
                    {
                        'title': 'text',
                        'text': '',
                        'content_type': 'textcontent',
                    },
                    {
                        'title': 'audio',
                        'bitrate': 128,
                        'content_type': 'audiocontent',
                    },
                    {
                        'title': 'video2',
                        'video': None,
                        'subtitles': None,
                        'content_type': 'videocontent',
                    },
                    {
                        'title': 'video1',
                        'video': None,
                        'subtitles': None,
                        'content_type': 'videocontent',
                    },
                ],
                'resource_url': f'/api/v1/pages/{page.pk}/',
            },
            serializer.data
        )


class ViewTest(APITestCase):
    def test_list_view(self):
        page = Page()
        page.save()
        video = VideoContent()
        video.save()
        page_to_content = PageToContent(page=page, content=video)
        page_to_content.ordering = 1
        page_to_content.save()
        response = self.client.get(reverse('api_v1_page-list'))
        self.assertEqual(200, response.status_code)
        self.assertEqual(
            {
                'count': 1,
                'next': None,
                'previous': None,
                'results': [
                    {
                        'title': '',
                        'contents': [
                            {
                                'title': '',
                                'video': None,
                                'subtitles': None,
                                'content_type': 'videocontent',
                            },
                        ],
                        'resource_url': f'http://testserver/api/v1/pages/{page.pk}/',
                    },
                ],
            },
            response.data
        )

    def test_detail_view(self):
        page = Page()
        page.save()
        video = VideoContent()
        video.save()
        page_to_content = PageToContent(page=page, content=video)
        page_to_content.ordering = 1
        page_to_content.save()
        response = self.client.get(reverse('api_v1_page-detail', args=[page.pk]))
        self.assertEqual(200, response.status_code)
        self.assertEqual(
            {
                'title': '',
                'contents': [
                    {
                        'title': '',
                        'video': None,
                        'subtitles': None,
                        'content_type': 'videocontent',
                    },
                ],
                'resource_url': f'http://testserver/api/v1/pages/{page.pk}/',
            },
            response.data
        )

    def test_get_queryset(self):
        pages = [Page.objects.create(title=f'page {n}') for n in range(3)]
        queryset = get_queryset()
        self.assertEqual(len(pages), queryset.count())

    @patch('app.views.update_counters.delay')
    def test_retrieve_call_update_counters(self, update_counters_delay):
        pages = [Page.objects.create(title=f'Page {n}') for n in range(5)]
        videos = [VideoContent.objects.create(title=f'Video {n}') for n in range(5)]
        audio = [AudioContent.objects.create(title=f'Audio {n}', bitrate=128) for n in range(5)]
        text = [TextContent.objects.create(title=f'Text {n}') for n in range(5)]

        p2c1 = PageToContent.objects.create(page=pages[0], content=videos[0], ordering=1)

        p2c2 = PageToContent.objects.create(page=pages[0], content=audio[0], ordering=2)
        PageToContent.objects.create(page=pages[1], content=audio[1], ordering=1)

        p2c3 = PageToContent.objects.create(page=pages[0], content=text[2], ordering=3)
        PageToContent.objects.create(page=pages[1], content=text[1], ordering=2)
        PageToContent.objects.create(page=pages[2], content=text[0], ordering=1)

        p2c4 = PageToContent.objects.create(page=pages[0], content=videos[4], ordering=4)
        PageToContent.objects.create(page=pages[1], content=videos[1], ordering=3)
        PageToContent.objects.create(page=pages[2], content=videos[2], ordering=2)
        PageToContent.objects.create(page=pages[3], content=videos[3], ordering=1)

        p2c5 = PageToContent.objects.create(page=pages[0], content=audio[4], ordering=5)
        PageToContent.objects.create(page=pages[1], content=videos[0], ordering=4)
        PageToContent.objects.create(page=pages[2], content=text[4], ordering=3)
        PageToContent.objects.create(page=pages[3], content=audio[0], ordering=2)
        PageToContent.objects.create(page=pages[4], content=videos[0], ordering=1)

        self.client.get(reverse('api_v1_page-detail', args=[pages[0].pk]))
        self.assertTrue(update_counters_delay.called)
        self.assertEqual(
            sorted([p2c1.content_id, p2c2.content_id, p2c3.content_id, p2c4.content_id, p2c5.content_id]),
            sorted(update_counters_delay.call_args[0][0])
        )

    def test_db_query_counts(self):
        for x in (1, 3, 5):
            pages = [Page.objects.create(title=f'Page {n}') for n in range(x)]
            videos = [VideoContent.objects.create(title=f'Video {n}') for n in range(x)]
            audios = [AudioContent.objects.create(title=f'Audio {n}', bitrate=128) for n in range(x)]
            texts = [TextContent.objects.create(title=f'Text {n}') for n in range(x)]

            for n, page in enumerate(pages):
                PageToContent.objects.create(page=page, content=videos[n], ordering=n + 1)
                PageToContent.objects.create(page=page, content=audios[n], ordering=n + 1)
                PageToContent.objects.create(page=page, content=texts[n], ordering=n + 1)

            with self.assertNumQueries(8):
                self.client.get(reverse('api_v1_page-list'))


class TaskTest(TestCase):
    def test_update_counter(self):
        video = VideoContent()
        video.save()
        update_counters([video.contentitem_ptr_id])
        video.refresh_from_db()
        self.assertEqual(1, video.counter)

    def test_update_counters(self):
        video = VideoContent()
        video.save()
        audio = AudioContent(bitrate=128)
        audio.save()
        text = TextContent()
        text.save()
        update_counters([video.contentitem_ptr_id, audio.contentitem_ptr_id, text.contentitem_ptr_id])
        video.refresh_from_db()
        audio.refresh_from_db()
        text.refresh_from_db()
        self.assertEqual(1, video.counter)
        self.assertEqual(1, audio.counter)
        self.assertEqual(1, text.counter)
