from rest_framework import mixins, viewsets, pagination
from rest_framework.response import Response

from app.models import Page, PAGE_CONTENT_ITEM_TYPES
from app.serializers import PageSerializer
from app.tasks import update_counters


class PagePageNumberPagination(pagination.PageNumberPagination):
    page_size = 10


def get_queryset():
    return Page.objects.prefetch_related(
        'pagetocontent_set',
        'pagetocontent_set__content',
        *[f'pagetocontent_set__content__{x.get_content_type()}' for x in PAGE_CONTENT_ITEM_TYPES]
    )


class PageViewSet(mixins.RetrieveModelMixin,
                  mixins.ListModelMixin,
                  viewsets.GenericViewSet):
    pagination_class = PagePageNumberPagination
    serializer_class = PageSerializer

    def get_queryset(self):
        return get_queryset()

    def retrieve(self, request, *args, **kwargs):
        page = self.get_object()
        serializer = self.get_serializer(page)
        content_ids = list(page.pagetocontent_set.values_list('content_id', flat=True))
        update_counters.delay(content_ids)
        return Response(serializer.data)
