from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include

from app.routes import api_v1

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/v1/', include(api_v1.urls)),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
