# Development:

- Build dev image:
```bash
docker-compose -f docker-compose-dev.yml build
```

- Run `runserver` command:
```bash
docker-compose -f docker-compose-dev.yml up
```
note: The application will be available at http://localhost:8000

- Run `makemigrations` command:
```bash
docker-compose -f docker-compose-dev.yml run --rm app ./manage.py makemigrations
```

- Run `migrate` command:
```bash
docker-compose -f docker-compose-dev.yml run --rm app ./manage.py migrate
```

# Running tests:

- Build test image:
```bash
docker-compose -f docker-compose-test.yml build
```

- Run `test` command:
```bash
docker-compose -f docker-compose-test.yml up
```

# Deployment example:

- Build prd image:
```bash
docker-compose -f docker-compose-prd-example.yml build
```

- Run application stack:
```bash
docker-compose -f docker-compose-prd-example.yml up
```
note: The application will be available at http://localhost

- Run `migrate` command:
```bash
docker-compose -f docker-compose-prd-example.yml run --rm app ./manage.py migrate
```
